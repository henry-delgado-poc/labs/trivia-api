﻿using trivia.Models;
using trivia.Requests;
using trivia.Responses;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace trivia.Services
{
    public interface IQuestionService
    {
        Task<Results<Question>> GetRandomList(GetRandomListRequest request);
        Task<Results<Question>> GetAll(GetByParentIdRequest request);

        Task<Result<Question>> GetById(GetOrDeleteBaseRequest request);

        Task<Result<Question>> Add(AddOrUpdateBaseRequest<Question> request);
        Task<Result<Question>> Update(AddOrUpdateBaseRequest<Question> request);
        Task Delete(GetOrDeleteBaseRequest request);

    }
}
