﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using trivia.CustomExceptions;
using trivia.Models;
using trivia.Requests;
using trivia.Responses;

namespace trivia.Services
{
    public class AnswerService : IAnswerService
    {
        private readonly ApplicationDbContext _context;

        public AnswerService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Result<Answer>> Add(AddOrUpdateBaseRequest<Answer> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Text))
                throw new ArgumentNullException($"{nameof(request)}");

            var questionFound = await _context.Question.FirstOrDefaultAsync(x => x.Id == request.Data.QuestionId) != null;
            if (!questionFound)
                throw new ArgumentException(nameof(request.Data.QuestionId));

            var found = await _context.Answer.FirstOrDefaultAsync(x => x.QuestionId == request.Data.QuestionId &&  x.Text.ToUpper() == request.Data.Text.ToUpper()) != null;
            if (found)
                throw new DuplicateFoundException($"{nameof(request.Data.Text)}");

            await _context.Answer.AddAsync(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Answer>(request.Data, request.CorrelationId);
        }

        public async Task Delete(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Answer.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            _context.Answer.Remove(data);
            await _context.SaveChangesAsync();
        }

        public async Task<Results<Answer>> GetAll(GetByParentIdRequest request)
        {
            if (request.ParentId < -1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Answer.Where(q => q.QuestionId == request.ParentId).OrderBy(x => Guid.NewGuid()).ToListAsync();
            return new Results<Answer>(data, request.CorrelationId);
        }

        public async Task<Result<Answer>> GetById(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Answer.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            return new Result<Answer>(data, request.CorrelationId);
        }

        public async Task<Result<Answer>> Update(AddOrUpdateBaseRequest<Answer> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Text))
                throw new ArgumentNullException($"{nameof(request)}");

            if (request?.Data.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var existingRecord = await _context.Subject.FirstOrDefaultAsync(x => x.Id == request.Data.Id);
            if (existingRecord == null)
                throw new ArgumentException($"{nameof(request.Data.Id)}");

            _context.Entry(existingRecord).CurrentValues.SetValues(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Answer>(request.Data, request.CorrelationId);
        }
    }
}
