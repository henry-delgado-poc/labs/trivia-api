﻿using trivia.Models;
using trivia.Requests;
using trivia.Responses;
using System.Threading.Tasks;
namespace trivia.Services
{
    public interface ITopicService
    {
        Task<Results<Topic>> GetRandomList(GetRandomListRequest request);
        Task<Results<Topic>> GetAll(GetByParentIdRequest request);
        Task<Result<Topic>> GetById(GetOrDeleteBaseRequest request);
        
        Task<Result<Topic>> Add(AddOrUpdateBaseRequest<Topic> request);
        Task<Result<Topic>> Update(AddOrUpdateBaseRequest<Topic> request);
        Task Delete(GetOrDeleteBaseRequest request);
    }
}
