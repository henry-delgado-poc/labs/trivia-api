﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using trivia.CustomExceptions;
using trivia.Models;
using trivia.Requests;
using trivia.Responses;

namespace trivia.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<QuestionService> _logger;

        public QuestionService(ApplicationDbContext context, ILogger<QuestionService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result<Question>> Add(AddOrUpdateBaseRequest<Question> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Text))
                throw new ArgumentNullException($"{nameof(request)}");

            var topicFound = await _context.Topic.FirstOrDefaultAsync(x => x.Id == request.Data.TopicId) != null;
            if (!topicFound)
                throw new ArgumentException(nameof(request.Data.TopicId));

            var found = await _context.Question.FirstOrDefaultAsync(x => x.TopicId == request.Data.TopicId && x.Text.ToUpper() == request.Data.Text.ToUpper()) != null;
            if (found)
                throw new DuplicateFoundException($"{nameof(request.Data.Text)}");

            await _context.Question.AddAsync(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Question>(request.Data, request.CorrelationId);

        }

        public async Task Delete(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Question.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            _context.Question.Remove(data);
            await _context.SaveChangesAsync();

        }

        public async Task<Results<Question>> GetAll(GetByParentIdRequest request)
        {
            if (request.ParentId < -1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Question.Where(q => q.TopicId == request.ParentId).OrderBy(x => Guid.NewGuid()).ToListAsync();
            return new Results<Question>(data, request.CorrelationId);

        }

        public async Task<Result<Question>> GetById(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Question.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            return new Result<Question>(data, request.CorrelationId);

        }

        public async Task<Results<Question>> GetRandomList(GetRandomListRequest request)
        {
            if (request.Count < 1)
                request.Count = 1;

            if (request.ParentId < 1)
                throw new ArgumentNullException(nameof(request.ParentId));

            var data = await _context.Question.Where(x => x.TopicId == request.ParentId).OrderBy(x => Guid.NewGuid()).Take(request.Count).ToListAsync();
            return new Results<Question>(data, request.CorrelationId);

        }

        public async Task<Result<Question>> Update(AddOrUpdateBaseRequest<Question> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Text))
                throw new ArgumentNullException($"{nameof(request)}");

            if (request?.Data.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var existingRecord = await _context.Subject.FirstOrDefaultAsync(x => x.Id == request.Data.Id);
            if (existingRecord == null)
                throw new ArgumentException($"{nameof(request.Data.Id)}");

            _context.Entry(existingRecord).CurrentValues.SetValues(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Question>(request.Data, request.CorrelationId);
        }
    }
}
