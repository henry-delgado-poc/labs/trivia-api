﻿using trivia.Models;
using trivia.Requests;
using trivia.Responses;
using System.Threading.Tasks;

namespace trivia.Services
{
    public interface IAnswerService
    {
        Task<Results<Answer>> GetAll(GetByParentIdRequest request);
        Task<Result<Answer>> GetById(GetOrDeleteBaseRequest request);

        Task<Result<Answer>> Add(AddOrUpdateBaseRequest<Answer> request);
        Task<Result<Answer>> Update(AddOrUpdateBaseRequest<Answer> request);
        Task Delete(GetOrDeleteBaseRequest request);
    }
}
