﻿using trivia.Models;
using trivia.Requests;
using trivia.Responses;
using System.Threading.Tasks;

namespace trivia.Services
{
    public interface ISubjectService
    {
        Task<Results<Subject>> GetAll(BaseRequest request);
        Task<Result<Subject>> GetById(GetOrDeleteBaseRequest request);

        Task<Result<Subject>> Add(AddOrUpdateBaseRequest<Subject> request);
        Task<Result<Subject>> Update(AddOrUpdateBaseRequest<Subject> request);
        Task Delete(GetOrDeleteBaseRequest request);
    }
}
