﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using trivia.CustomExceptions;
using trivia.Models;
using trivia.Requests;
using trivia.Responses;

namespace trivia.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly ApplicationDbContext _context;

        public SubjectService( ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Result<Subject>> Add(AddOrUpdateBaseRequest<Subject> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Name))
                throw new ArgumentNullException($"{nameof(request)}");

            var found = await _context.Subject.FirstOrDefaultAsync(x => x.Name.ToUpper() == request.Data.Name.ToUpper()) != null;
            if (found)
                throw new DuplicateFoundException($"{nameof(request.Data.Name)}");

            await _context.Subject.AddAsync(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Subject>(request.Data, request.CorrelationId);

        }

        public async Task Delete(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Subject.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            _context.Subject.Remove(data);
            await _context.SaveChangesAsync();

        }

        public async Task<Result<Subject>> GetById(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Subject.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            return new Result<Subject>(data, request.CorrelationId);

        }

        public async Task<Results<Subject>> GetAll(BaseRequest request)
        {
            var data = await _context.Subject.OrderBy(x => Guid.NewGuid()).ToListAsync();
            return new Results<Subject>(data, request.CorrelationId);

        }

        public async Task<Result<Subject>> Update(AddOrUpdateBaseRequest<Subject> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Name))
                throw new ArgumentNullException($"{nameof(request)}");

            if (request?.Data.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var existingRecord = await _context.Subject.FirstOrDefaultAsync(x => x.Id == request.Data.Id);
            if (existingRecord == null)
                throw new ArgumentException($"{nameof(request.Data.Id)}");

            _context.Entry(existingRecord).CurrentValues.SetValues(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Subject>(request.Data, request.CorrelationId);

        }
    }
}
