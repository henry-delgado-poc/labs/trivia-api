﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using trivia.CustomExceptions;
using trivia.Models;
using trivia.Requests;
using trivia.Responses;
using trivia.Services;

namespace trivia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswersController : ControllerBase
    {
        private readonly IAnswerService _service;
        private readonly ILogger<AnswersController> _logger;

        public AnswersController(IAnswerService service, ILogger<AnswersController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<Results<Answer>>> Get([FromQuery] GetByParentIdRequest request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.CorrelationId))
                    request.CorrelationId = Guid.NewGuid().ToString();

                _logger.LogInformation($"Getting {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.GetAll(request);
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error getting {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpGet]
        [Route("GetById")]
        public async Task<ActionResult<Result<Answer>>> GetById([FromQuery] GetOrDeleteBaseRequest request)
        {
            try
            {
                _logger.LogInformation($"Getting {nameof(Answer)} by id [{request.Id}]. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.GetById(request);
                return Ok(result);
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error getting {nameof(Answer)} by Id. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error getting {nameof(Answer)} by Id. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error getting {nameof(Answer)} by Id. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpPost]
        public async Task<ActionResult<Answer>> Post(AddOrUpdateBaseRequest<Answer> request)
        {
            try
            {
                _logger.LogInformation($"Creating new {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.Add(request);
                return Ok(result);
            }
            catch (DuplicateFoundException de)
            {
                _logger.LogInformation($"{nameof(Answer)} was not added due to possible duplicate: [{de.Argument}]. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}, de");
                return BadRequest("Possible duplicate found");
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error creating {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error creating {nameof(Answer)}. {nameof(Question)} not found. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error creating {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpPut]
        public async Task<ActionResult<Answer>> Put(AddOrUpdateBaseRequest<Answer> request)
        {
            try
            {
                _logger.LogInformation($"Updating {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.Update(request);
                return Ok(result);
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error updating {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error updating {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error updating {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(GetOrDeleteBaseRequest request)
        {
            try
            {
                _logger.LogInformation($"Deleting {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");
                await _service.Delete(request);
                return Ok();
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error deleting {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error deleting record: {nameof(Answer)} not found. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error deleting {nameof(Answer)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }
    }
}
