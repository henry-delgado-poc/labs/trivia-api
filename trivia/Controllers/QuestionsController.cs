﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using trivia.CustomExceptions;
using trivia.Models;
using trivia.Requests;
using trivia.Responses;
using trivia.Services;

namespace trivia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionsController : ControllerBase
    {
        private readonly IQuestionService _service;
        private readonly ILogger<QuestionsController> _logger;

        public QuestionsController(IQuestionService service, ILogger<QuestionsController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet]
        [Route("GetRandomList")]
        public async Task<ActionResult<Results<Question>>> GetRandomList([FromQuery] GetRandomListRequest request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.CorrelationId))
                    request.CorrelationId = Guid.NewGuid().ToString();

                _logger.LogInformation($"Getting random list of {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.GetRandomList(request);
                return Ok(result);
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Missing {an.Message}", an);
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error getting random list of {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpGet]
        public async Task<ActionResult<Results<Question>>> Get([FromQuery] GetByParentIdRequest request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.CorrelationId))
                    request.CorrelationId = Guid.NewGuid().ToString();

                _logger.LogInformation($"Getting list of all {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.GetAll(request);
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error getting list of all {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpGet]
        [Route("GetById")]
        public async Task<ActionResult<Results<Question>>> GetById([FromQuery] GetOrDeleteBaseRequest request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.CorrelationId))
                    request.CorrelationId = Guid.NewGuid().ToString();

                _logger.LogInformation($"Getting {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.GetById(request);
                return Ok(result);
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error getting {nameof(Question)} by Id. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error getting {nameof(Question)} by Id. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error getting {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpPost]
        public async Task<ActionResult<Question>> Post(AddOrUpdateBaseRequest<Question> request)
        {
            try
            {
                _logger.LogInformation($"Creating new {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.Add(request);
                return Ok(result);
            }
            catch (DuplicateFoundException de)
            {
                _logger.LogInformation($"{nameof(Question)} was not added due to possible duplicate: [{de.Argument}]. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}, de");
                return BadRequest("Possible duplicate found");
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error creating {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error creating {nameof(Question)}. {nameof(Topic)} not found. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return BadRequest();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error creating {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpPut]
        public async Task<ActionResult<Question>> Put(AddOrUpdateBaseRequest<Question> request)
        {
            try
            {
                _logger.LogInformation($"Updating {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");

                var result = await _service.Update(request);
                return Ok(result);
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error updating {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error updating {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error updating {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(GetOrDeleteBaseRequest request)
        {
            try
            {
                _logger.LogInformation($"Deleting {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}");
                await _service.Delete(request);
                return Ok();
            }
            catch (ArgumentNullException an)
            {
                _logger.LogError($"Error deleting {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", an);
                return BadRequest();
            }
            catch (ArgumentException ae)
            {
                _logger.LogError($"Error deleting record: {nameof(Question)} not found. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", ae);
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError($"Error deleting {nameof(Question)}. {Utils.GetCorrelationIdLogEntry(request.CorrelationId)}", e);
                throw new Exception("An error occurred on the server");
            }
        }
    }
}
