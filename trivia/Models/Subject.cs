﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace trivia.Models
{
    public class Subject
    {
        [Key]
        public long Id { get; set; }
        
        [MaxLength(300)]
        public string Name { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }

        public Subject()
        {

        }

        public Subject(long id, string name)
        {
            Id = id;
            Name = name;
        }

        public Subject(string name)
        {
            Name = name;
        }
    }
}
