﻿using System.ComponentModel.DataAnnotations;

namespace trivia.Models
{
    public class Answer
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public long QuestionId { get; set; }
        [MaxLength(300)]
        public string Text { get; set; }
        public virtual Question Question { get; set; }
        public bool IsCorrect { get; set; }
    }
}
