﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace trivia.Models
{
    public class Topic
    {
        [Key]
        public long Id { get; set; }

        [Required]
        [MaxLength(300)]
        public string Name { get; set; }

        public long SubjectId { get; set; }

        public virtual Subject Subject { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

        public Topic(long id, string name)
        {
            Id = id;
            Name = name;
        }

        public Topic()
        {

        }

    }
}
