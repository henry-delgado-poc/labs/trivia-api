﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace trivia.Models
{
    public class Question
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public long TopicId { get; set; }

        public virtual Topic Topic { get; set; }

        [MaxLength(800)]
        public string Text { get; set; }

        [MaxLength(800)]
        public string AnswerExplanation { get; set; }

        [DataType(DataType.Url)]
        public string AnswerUrlReference { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        public virtual ICollection<QuestionStat> QuestionStats { get; set; }

        public Question(long id, long topicId, string text, string explanation, string url)
        {
            Id = id;
            TopicId = topicId;
            Text = text;
            AnswerExplanation = explanation;
            AnswerUrlReference = url;
        }

        public Question()
        {

        }

    }
}
