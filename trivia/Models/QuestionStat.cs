﻿using System;
using System.ComponentModel.DataAnnotations;

namespace trivia.Models
{
    public class QuestionStat
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public long QuestionId { get; set; }

        public virtual Question Question { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; } = DateTime.UtcNow;

        [MaxLength(200)]
        public string Player { get; set; }

        public bool AnsweredCorrectly { get; set; }

        public QuestionStat(long id, long questionId, string player, bool answeredCorrectly)
        {
            Id = id;
            QuestionId = questionId;
            Date = DateTime.UtcNow;
            Player = player;
            AnsweredCorrectly = answeredCorrectly;
        }

        public QuestionStat()
        {
            Date = DateTime.UtcNow;
        }

    }
}
