﻿using System;

namespace trivia.CustomExceptions
{
    public class DuplicateFoundException: SystemException
    {
        public string Argument { get; set; }

        public DuplicateFoundException()
        {

        }

        public DuplicateFoundException(string argument)
        {
            Argument = argument;
        }
    }
}
