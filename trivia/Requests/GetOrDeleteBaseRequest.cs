﻿namespace trivia.Requests
{
    public class GetOrDeleteBaseRequest: BaseRequest
    {
        public long Id { get; set; }
    }
}
