﻿namespace trivia.Requests
{
    public class GetByParentIdRequest: BaseRequest
    {
        public long ParentId { get; set; }
    }
}
