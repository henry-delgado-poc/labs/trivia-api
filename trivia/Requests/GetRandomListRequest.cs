﻿namespace trivia.Requests
{
    public class GetRandomListRequest : BaseRequest
    {
        public int Count { get; set; }

        public long ParentId { get; set; }
    }

}
