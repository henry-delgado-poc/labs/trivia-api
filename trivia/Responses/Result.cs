﻿namespace trivia.Responses
{
    public class Result<T>
    {
        public T Data { get; set; }
        public string CorrelationId { get; set; }

        public Result()
        {

        }

        public Result(T data, string correlationId)
        {
            this.Data = data;
            this.CorrelationId = correlationId;
        }
    }

}
