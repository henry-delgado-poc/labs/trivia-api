﻿using System.Linq;
using trivia.Models;

namespace trivia.Responses
{
    public class QuestionsLookupResponse
    {
        public string CorrelationId { get; set; }

        public ILookup<long, Question> Lookup { get; set; }
    }
}
