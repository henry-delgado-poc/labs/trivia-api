﻿using Microsoft.EntityFrameworkCore;
using trivia.Models;

namespace trivia
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Subject> Subject { get; set; }
        public DbSet<Topic> Topic { get; set; }
        public DbSet<Question> Question { get; set; }
        public DbSet<Answer> Answer { get; set; }
        public DbSet<QuestionStat> QuestionStat { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("trivia");

            modelBuilder.Entity<Subject>().HasIndex(x => x.Name);
            modelBuilder.Entity<Topic>().HasIndex(x => x.Name);
            modelBuilder.Entity<Question>().HasIndex(x => x.Text);
            modelBuilder.Entity<Answer>().HasIndex(x => x.Text);

            modelBuilder.Entity<Answer>()
                .HasOne(x => x.Question)
                .WithMany(x => x.Answers)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Question>()
                .HasOne(x => x.Topic)
                .WithMany(x => x.Questions)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<QuestionStat>()
                .HasOne(x => x.Question)
                .WithMany(x => x.QuestionStats)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Topic>()
                .HasOne(x => x.Subject)
                .WithMany(x => x.Topics)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }

    }
}
