﻿namespace trivia
{
    public static class Utils
    {
        public static string GetCorrelationIdLogEntry(string correlationId)
        {
            return $"CorrelationId:[{correlationId}]";
        }
    }
}
