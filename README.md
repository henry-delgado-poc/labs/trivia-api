# Trivia API

A generic Trivia API that Allows users to add questions and related answers for any subject and topic.

![Diagram](./Trivia.png)

**Subject:** The describe the name for the associated topics. For example. Bible Trivia, Chemistry Trivia, Goverment 101, etc.

**Topic:** The topic associated to the given subject. For example: A Bible trivia might have topics such as history, miracles, etc.

**Question:** To manage the questions to ask for any given topic. One topic has many questions.

**Answer:** To manage all correct and incorrect answers to display for any given question. One question has many answers but only one is correct.

**Question Stats:** Keeps track of what questions have been asked